package com.sync.async;

public class HostnameResolutionException extends Exception {
    public HostnameResolutionException(String message) {
        super(message);
    }
}
