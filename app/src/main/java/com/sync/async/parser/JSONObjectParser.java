package com.sync.async.parser;

import com.sync.async.DataEmitter;
import com.sync.async.DataSink;
import com.sync.async.callback.CompletedCallback;
import com.sync.async.future.Future;
import com.sync.async.future.TransformFuture;
import org.json.JSONObject;

import java.lang.reflect.Type;


public class JSONObjectParser implements AsyncParser<JSONObject> {
    @Override
    public Future<JSONObject> parse(DataEmitter emitter) {
        return new StringParser().parse(emitter)
        .then(new TransformFuture<JSONObject, String>() {
            @Override
            protected void transform(String result) throws Exception {
                setComplete(new JSONObject(result));
            }
        });
    }

    @Override
    public void write(DataSink sink, JSONObject value, CompletedCallback completed) {
        new StringParser().write(sink, value.toString(), completed);
    }

    @Override
    public Type getType() {
        return JSONObject.class;
    }
}
