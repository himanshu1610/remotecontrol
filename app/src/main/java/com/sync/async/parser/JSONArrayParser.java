package com.sync.async.parser;

import com.sync.async.DataEmitter;
import com.sync.async.DataSink;
import com.sync.async.callback.CompletedCallback;
import com.sync.async.future.Future;
import com.sync.async.future.TransformFuture;

import org.json.JSONArray;

import java.lang.reflect.Type;

public class JSONArrayParser implements AsyncParser<JSONArray> {
    @Override
    public Future<JSONArray> parse(DataEmitter emitter) {
        return new StringParser().parse(emitter)
        .then(new TransformFuture<JSONArray, String>() {
            @Override
            protected void transform(String result) throws Exception {
                setComplete(new JSONArray(result));
            }
        });
    }

    @Override
    public void write(DataSink sink, JSONArray value, CompletedCallback completed) {
        new StringParser().write(sink, value.toString(), completed);
    }

    @Override
    public Type getType() {
        return JSONArray.class;
    }
}
